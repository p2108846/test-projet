@extends('layouts.layout')

<link rel= "stylesheet" href= "css/manga.css">

@section('titrePage')
    Liste des Dessinateur
@endsection

@section('titreItem')
   <h1> Tous les Dessinateur </h1>
@endsection

@section('contenu')
<table class="table table-bordered table-striped">
    <thead>
        <th>id_dessinateur</th>
        <th>nom_dessinateur</th>
        <th>prenom_dessinateur</th>
    </thead>
    @foreach($dessinateurs as $dessinateur)
        <tr>
            <td> {{ $dessinateur->id_dessinateur }} </td>
            <td> {{ $dessinateur->nom_dessinateur }} </td>
            <td> {{ $dessinateur->prenom_dessinateur }} </td>
        </tr>
    @endforeach
</table>
@endsection