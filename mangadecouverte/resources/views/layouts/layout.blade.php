<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/mangas.css">
    <title>
        @yield('titrePage')
    </title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">MangaWorld</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link active" aria-current="page" href="{{url('/mangas')}}">Home </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://www.senscritique.com/musique/actualite">Musique</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://www.senscritique.com/bd/actualite/manga">Manga</a>
      </li>
    </ul>
  </div>
</nav>
    <header>
        @yield('titreItem')
    </header>
    @yield('contenu')
    <footer class= "footer">
        MangaWeb - copyright 3AInfo -2021
    </footer>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" > </script>
</body>
</html>