<?php

namespace App\Http\Controllers;

//use App\Http\Requests\StoreMangaRequest;
//use App\Http\Requests\UpdateMangaRequest;
use Illuminate\Http\Request;
use App\Models\Dessinateur;

class DessinateurController extends Controller
{
    public function index()
    {
        $dessinateur = new Dessinateur();
        $dessinateurs = $dessinateur->getAll();
        return view('index2',compact('dessinateurs'));
    }
}